<?php
/**
 * @file
 * Settings overview for the locked permissions.
 * 
 * @author Nicholas Alipaz - http://nicholas.alipaz.net/ (email through contact form)
 */

function permissions_lock_in_settings_settings_form($form, &$form_state) {
  $form = array();
  $permission_locks = permissions_lock_in_settings_permission_construct_list_array(permissions_lock_in_settings_get_locked('permission'));
  $role_locks = permissions_lock_in_settings_role_construct_list_array(permissions_lock_in_settings_get_locked('role'));

  $form['permissions_lock_in_settings_permission'] = array(
    '#type' => 'fieldset',
    '#title' => t('Permissions'),
    '#weight' => 0,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  if ($permission_locks) {
    $form['permissions_lock_in_settings_permission']['permissions_lock_in_settings_permission_locks'] = array(
      '#theme' => 'item_list',
      '#title' => t('Locked permissions in settings file'),
      '#items' => $permission_locks,
      '#type' => 'ul',
      '#description' => t('A list of permissions that are locked through the settings file.'),
    );
  }
  else {  
    $form['permissions_lock_in_settings_permission']['message'] = array(
      '#markup' => t('No permissions are currently being locked through the settings file.')
    );
  }

  $form['permissions_lock_in_settings_role'] = array(
    '#type' => 'fieldset',
    '#title' => t('Roles'),
    '#weight' => 0,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  if ($role_locks) {
    $form['permissions_lock_in_settings_role']['permissions_lock_in_settings_role_locks'] = array(
      '#theme' => 'item_list',
      '#title' => t('Locked roles in settings file'),
      '#items' => $role_locks,
      '#type' => 'ul',
      '#description' => t('A list of roles that are locked through the settings file.'),
    );
  }
  else {
    $form['permissions_lock_in_settings_role']['message'] = array(
      '#markup' => t('No roles are currently being locked through the settings file.')
    );
  }

  return system_settings_form($form);
}

function permissions_lock_in_settings_role_construct_list_array($list) {
  $items = array();
  $all_roles = user_roles();

  foreach ($list as $rid => $value) {
    $items[$rid] = array(
      'data' => $all_roles[$rid],
    );
  }

  return $items;
}

function permissions_lock_in_settings_permission_construct_list_array($list) {
  $items = array();
  $all_roles = user_roles();

  foreach ($list as $permission => $roles) {
    $items[$permission] = array(
      'data' => $permission,
    );
    if (!empty($all_roles)) {
      $items[$permission]['children'] = array();
    }
    foreach ($roles as $rid => $locked_value) {
      $locked_value = ($locked_value) ? 'enabled' : 'disabled';
      if ($rid === 'all') {
        foreach ($all_roles as $all_rid => $all_role) {
          $items[$permission]['children'][$all_rid] = array(
            'data' => $all_role . ' (' . $locked_value . ')',
          );
        }
      }
      else {
        $items[$permission]['children'][$all_roles[$rid]] = array(
          'data' => $all_roles[$rid] . ' (' . $locked_value . ')',
        );
      }
    }
  }

  return $items;
}
