<?php
/**
 * @file
 * Form functions for module
 * 
 * Central file to place all form alterations and related functions.
 * @author Nicholas Alipaz - http://nicholas.alipaz.net/ (email through contact form)
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function permissions_lock_in_settings_form_user_admin_permissions_alter(&$form, &$form_state, $form_id) {
  $form['#after_build'][] = 'permissions_lock_in_settings_form_alteration_callback';
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function permissions_lock_in_settings_form_user_permissions_profile_permissions_form_alter(&$form, &$form_state, $form_id) {
  $form['#after_build'][] = 'permissions_lock_in_settings_form_user_permissions_profile_permissions_form_after_build';
  $form['#after_build'][] = 'permissions_lock_in_settings_form_alteration_callback';
}

/**
 * Callback for permissions_lock_in_settings_form_user_permissions_profile_permissions_form_alter().
 */
function permissions_lock_in_settings_form_user_permissions_profile_permissions_form_after_build($form_element, &$form_state) {
  $locked_roles = permissions_lock_in_settings_get_locked('role');

  if (!empty($locked_roles)) {
    /**
     * If we are on a role-specific page, we redirect user to general 
     * permissions page, as there is nothing to change on the specific page 
     * anyway.
     */
    $editing_rid = arg(3);

    if (is_numeric($editing_rid) && $editing_rid > 0 && in_array($editing_rid, $locked_roles)) {
      drupal_goto('admin/people/permissions');
    }
  }

  // If we make it this far without redirecting then it will fall through to the
  // the next after_build and make the changes to permissions in the form.
  return $form_element;
}

/**
 * Callback for permissions_lock_in_settings_form_user_permissions_profile_permissions_form_alter().
 */
function permissions_lock_in_settings_form_alteration_callback($form_element, &$form_state) {
  // Get all available permissions in the form
  $available_permissions = _permissions_lock_in_settings_get_form_available_permissions($form_element);
  // get locked roles & permissions, to remove them from the form
  $locked_roles = permissions_lock_in_settings_get_locked('role');
  $locked_permissions = permissions_lock_in_settings_get_locked('permission');
  // Get all the roles in the form.
  $available_roles = $form_element['role_names'];

  // add our custom validate handler, to restore unset permissions back to their current settings
  $form_element["#validate"][] = 'permissions_lock_in_settings_form_validate';

  // Now remove the permission rows for all locked permissions.
  if (!empty($available_roles) && !empty($available_permissions)) { // Make sure there is something left to unset.
    foreach ($available_permissions as $perm) {
      $locked_permission_keys = (isset($locked_permissions[$perm]) && is_array($locked_permissions[$perm])) ? array_keys($locked_permissions[$perm]) : array();

      foreach ($available_roles as $rid => $role) {
        if (strpos($rid, '#') === FALSE) {
          if (isset($locked_permissions[$perm]) && isset($locked_permissions[$perm]['all'])) {
            permissions_lock_in_settings_change_permission_in_form($form_element, $rid, $perm, $locked_permissions[$perm]['all']);
          }

          else if (isset($locked_permissions[$perm]) && in_array($rid, $locked_permission_keys)) {
            permissions_lock_in_settings_change_permission_in_form($form_element, $rid, $perm, $locked_permissions[$perm][$rid]);
          }

          else if (in_array($rid, $locked_roles)) {
            permissions_lock_in_settings_change_permission_in_form($form_element, $rid, $perm, in_array($perm, $form_element['checkboxes'][$rid]['#default_value']));
          }
        }
      }
    }
    _permissions_lock_in_settings_cleanup_orphan_headers($form_element);
  }
  
  return $form_element;
}

/**
 * 
 * @param type $form
 * @param type $rid
 * @param type $perm
 * @param type $value
 */
function permissions_lock_in_settings_change_permission_in_form(&$form_element, $rid, $perm, $value) {
  if (!user_access('view permission locks')) {
    unset($form_element['permission'][$perm]);
    unset($form_element['checkboxes'][$rid]['#options'][$perm]);
  }
  else {
    if ($value) {
      $form_element['checkboxes'][$rid][$perm]['#checked'] = $value;
      $form_element['checkboxes'][$rid][$perm]['#value'] = $perm;
      $form_element['checkboxes'][$rid]['#default_value'][] = $perm;
    }

    $form_element['checkboxes'][$rid][$perm]['#disabled'] = TRUE;
    $form_element['checkboxes'][$rid][$perm]['#attributes']['disabled'] = 'disabled';
    $form_element['checkboxes'][$rid][$perm]['#attributes']['title'] = t('The "@perm" permission has been locked through the settings file and requires developer access to alter.', array('@perm' => $perm));
  }
}

/**
 * Custom validation handler to keep the locked permissions in their current state.
 */
function permissions_lock_in_settings_form_validate($form, &$form_state) {
  // get locked roles, to remove them from the form_state
  $locked_roles = permissions_lock_in_settings_get_locked('role');

  // for roles that are locked, we remove their entry in $form_state
  if (!empty($locked_roles)) {
    foreach ($locked_roles as $locked_rid) {
      unset($form_state['values']['role_names'][$locked_rid]);
    }
  }
}

/**
 * Helper function to retrieve the permissions that are available in the permissions form
 */
function _permissions_lock_in_settings_get_form_available_permissions($form) {
  if (!empty($form['permission'])) {
    foreach ($form['permission'] as $permission => $value) {
      if (!is_numeric($permission)) {
        $available_permissions[] = $permission;
      }
    }
  }

  return $available_permissions;
}

/**
 * Helper function to remove permission table headers without related permissions
 */
function _permissions_lock_in_settings_cleanup_orphan_headers(&$form) {
  if (!empty($form['permission'])) {
    $prev_numeric = FALSE;
    $num = 0;

    foreach ($form['permission'] as $permission => $value) {
      if (is_numeric($permission)) {
        if ($prev_numeric) {
          // 2 numeric items following each other. Remove the first one, as it's an orphan header
          unset($form['permission'][$num]);
        }

        $prev_numeric = TRUE;
        $num = $permission;
      }
      else {
        $prev_numeric = FALSE;

        // Unset $num, needed for last iteration check
        unset($num);
      }
    }

    // After last iteration in foreach, check if last row was numeric
    // If so, remove, it is an orphan
    if (isset($num)) {
      unset($form['permission'][$num]);
    }
  }
}
